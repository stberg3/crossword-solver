import node_pb2
from Trie import Trie

class Guesser(object):
    def __init__(self, trie_path='words.proto'):
        n = node_pb2.Node()

        with open(trie_path, 'rb') as f:
            n.ParseFromString(f.read())

        self.trie_corpus = Trie(root=n)

    def guess(self, word, curr='', idx=0, root=None):
        if not root:
            root = self.trie_corpus.root

        if len(curr) == len(word):
            if root.term:
                print(curr)
            return

        if word[idx] == ' ':
            for c in root.children.keys():
                curr += chr(c)
                self.guess(word, curr, idx+1, root=root.children[c])
                curr = curr[:-1]
        else:
            curr += word[idx]
            self.guess(word, curr, idx+1, root=root.children[ord(word[idx])])

    def get_n_lengths(self, n, root=None, res=list(), prefix=''):
        if not root:
            root = self.trie_corpus.root

        if n == 0:
            if root.term:
                res.append(prefix)

            return res

        for child in root.children.keys():
            prefix += chr(child)
            self.get_n_lengths(n-1, root=root.children[child], res=res, prefix=prefix)
            prefix = prefix[:-1]

        return res


def main(ARGS):
    g = Guesser(trie_path=ARGS.corpus)
    g.guess(ARGS.word)

if __name__ == '__main__':
    DEFAULT_SAMPLE_RATE = 16000

    import argparse

    parser = argparse.ArgumentParser(description="Guess words!")

    parser.add_argument('-w', '--word', default='g ess', required=True,
                        help="String")

    parser.add_argument('-c', '--corpus', default='words.proto',
                        help="String")



    ARGS = parser.parse_args()
    main(ARGS)
