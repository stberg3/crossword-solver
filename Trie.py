import node_pb2

DEBUG = False

class Trie():

    tab = ''

    def _enter(self, msg):
        print('{} Entering... {}'.format(self.tab, msg))
        self.tab = self.tab + ' | '

    def _exit(self, msg):
        self.tab = self.tab[:-3]
        print('{} Exiting... {}'.format(self.tab, msg))        

    def __init__(self, words=list(), root=node_pb2.Node()):
        self.children = dict()
        self.root = root
        self.term = False

        for word in words:
            if word.isalpha():
                self.add_word(word)

    def add_word(self, word):
        return self._add_word(word.lower(), self.root)

    def _add_word(self, word, root):
        if DEBUG:
            self._enter('adding word "{}" - {}'.format(word, root))

        if word == '':
            root.term = True
            if DEBUG:
                self._exit('added word')
            return root

        res = None
        fl = ord(word[0])

        if root.children[fl]:
            res = self._add_word(word[1:], root.children[fl])
        else:
            root.children[fl] = Node()
            res = self._add_word(word[1:], root.children[fl])

        if DEBUG:
            self._exit('added word {}'.format(word))

        return res

    def __getitem__(self, word):
        return self.get_word(word, self.root)

    def get_word(self, word, root):
        if DEBUG:
            self._enter('getting word {} - {}'.format(word, root))

        if not root:
            if DEBUG:
                self._exit('tried getting word "{}"'.format(word))
            return False

        if word == '':
            if DEBUG:
                self._exit('tried getting word "{}"'.format(word))
            return root.term

        res = self.get_word(word[1:], root.children[ord(word[0])])

        if DEBUG:
            self._exit('tried getting word "{}"'.format(word))

        return res;
